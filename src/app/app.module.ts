import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component} from '@angular/core';


import { AppComponent } from './app.component';
import { ListsModule } from './listes/lists.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ListsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
