import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ListsService } from '../lists.service';

@Component({
  selector: 'app-list-edit',
  templateUrl: './list-edit.component.html',
  styleUrls: ['./list-edit.component.css']
})
export class ListEditComponent implements OnInit {

  form: FormGroup; //pour le formulaire

  constructor(private formBuilder:FormBuilder, 
              private listeService : ListsService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      listName: ['', Validators.required],
      listValue: this.formBuilder.array([])
    });
  }

  //méthode pour ajouter le tableau au formBuilder 
  onAddOptions() {
    const newOptionControl = this.formBuilder.control(null, Validators.required);//validators pour obliger a remplir le champ
    this.getOptions().push(newOptionControl);
  }

  getOptions() {
    return this.form.get('listValue') as FormArray;//retourne  le tableau de listes rempli dans newListe ci dessus
  }

  createList() {
    //récuperation données du formulaire : formValue
    const formValue = this.form.value;
    
    //création id
    const id = this.listeService.listes.length; //va chercher la liste du service (ok car id commence à 0)
    formValue['id'] = id;

    //création d'un tableau d'options avec création id et clé et récupérer name  dans formulaire formValue['listValue']
    const tabOptionForm = formValue['listValue'];//récupération des options rentrées dans le form dans un tableau
    let tableauOptions = [];//création d'un tableau vide

    for (let i = 0; i < tabOptionForm.length; i++) { //boucle pour remplir le tableau
      tableauOptions.push({ id: i, //remplissage du tableau avec un id, clé, name
                            cle: 'OPT'+(i+1) +' de liste '+(id+1),
                            name: tabOptionForm[i]+' de liste '+(id+1)
                    });
    }
    formValue['listValue'] = tableauOptions;

    this.listeService.addList(formValue); //transmission du tableau à addList (pr ajout à la liste)

    //Réinitialisation du formulaire : rappel du code de NgOnInit()
    this.form = this.formBuilder.group({
      listName: ['', Validators.required],
      listValue: this.formBuilder.array([])
    });
  }

// créer fonction pour récupérer les données du form de modif "recupUdate"


}
