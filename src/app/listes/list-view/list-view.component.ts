import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {

  @Input() listeInput : Object; //récupéré de lists.comp.html [ ] !! penser à le typer
  constructor() { }

  ngOnInit() {
  }

}
