import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListEditComponent } from './list-edit/list-edit.component';
import { ListViewComponent } from './list-view/list-view.component';
import { ListsComponent } from './lists/lists.component';
import { ListsService } from './lists.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FocusModule } from 'angular2-focus';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    FocusModule.forRoot()
  ],
  declarations: [ListEditComponent,ListViewComponent,ListsComponent],
  providers: [ListsService],
  exports:[ListsComponent]
})
export class ListsModule { }
