import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class ListsService {
  listSubject = new Subject();

  public listes = [
    {
        id: 0,
        listName: "Liste 1",
        listValue: [
            {
                id : 0,
                cle: 'OPT1 de liste1',
                name: 'option1 de liste1'
            },
            {
                id: 1,
                cle: 'OPT2 de liste1',
                name: 'option2 de liste1'
            },
            {
                id :2,
                cle: 'OPT3 de liste1',
                name: 'option3 de liste1'
            }
        ]
    },

    {
        id: 1,
        listName: "Liste 2",
        listValue: [
            {
                id :0,
                cle: 'OPT1 de liste2',
                name: 'option1 de liste2'
            },
            {
                id:1,
                cle: 'OPT2 de liste2',
                name: 'option2 de liste2'
            },
            {
                id :2,
                cle: 'OPT3 de liste2',
                name: 'option3 de liste2'
            }
        ]
    },
    {
        id: 2,
        listName: "Liste 3",
        listValue: [
            {
                id :0,
                cle: 'OPT1 de liste3',
                name: 'option1 de liste3'
            },
            {
                id :1,
                cle: 'OPT2 de liste3',
                name: 'option2 de liste3'
            },
            {
                id :2,
                cle: 'OPT3 de liste3',
                name: 'option3 de liste3'
            }
        ]
    },
    {
        id: 3,
        listName: "Liste 4",
        listValue: [
            {
                id :0,
                cle: 'OPT1 de liste4',
                name: 'option1 de liste4'
            },
            {
                id :1,
                cle: 'OPT2 de liste4',
                name: 'option2 de liste4'
            },
            {
                id : 2,
                cle: 'OPT3 de liste4',
                name: 'option3 de liste4'
            }
        ]
    }
]

  constructor() { }

  emitListes(){ //Observable / tuyau pour que les comp abonnés viennent récupérer
    this.listSubject.next(this.listes.slice())
}


addList(liste) { //ajout d'une nouvelle liste à lists
  this.listes.push(liste);
    this.emitListes(); //renvoi pour affichage de la liste complète
}

}
