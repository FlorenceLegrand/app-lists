import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ListsService } from '../lists.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  listes : any[]; //création d'une liste vide
  error = '';
  listeSubscription : Subscription;

  constructor(private listeService: ListsService) { }

  ngOnInit() {
    //création de la subscription en souscrivant (subscribe) au subject créé dans le service //équivaut à l'ancienne fonction getList()
    this.listeSubscription = this.listeService.listSubject.subscribe(
      (resultListes : any[]) => { this.listes = resultListes; }//il emet un array de type listes et on remplit le tableau créé ci-dessus
    
    );
    this.listeService.emitListes();//puis on émet le subject pr pouvoir afficher
  }

}
